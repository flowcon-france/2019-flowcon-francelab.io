+++
+++
<a class="anchor" id="about"></a>
<section class="pt50 pb50">
    <div class="container">
        <div class="section_title">
            <h3 class="title">
                A propos de FlowCon France
            </h3>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-12">
                <p style="text-align: justify;">
                    Après 6 éditions qui l'ont placée parmi les conférences de référence en France, Lean Kanban France est devenue FlowCon France.
                    Nous voulions une nouvelle identité plus en phase avec ce que nous voulons partager et promouvoir : le <b>flux</b>. 
                    Notre objectif est d'aider tous ceux qui veulent rendre leur organisation plus fluide.
                    FlowCon, c'est offrir des contenus de qualité, français et internationaux, dans un cadre chaleureux et informel autour de thèmes tels que le Kanban, l'agilité, la livraison continue, l'expérience utilisateur, la gestion décentralisée, etc.
                </p>
                <p>
                    Nous avons également le plaisir d'inviter Don Reinertsen, auteur de l'ouvrage fondateur sur les principes du développement de produits, à enseigner sa masterclass. <a href="/flow-masterclass/">Plus d'information ici</a>
                </p>
                <p>
                    FlowCon a également un meetup associé avec des événements réguliers : 
                    <a target="_blank" href="https://www.meetup.com/fr-FR/FlowCon-France-Meetup/">https://www.meetup.com/fr-FR/FlowCon-France-Meetup/</a>
                </p>
            </div>
        </div>
        <div class="row justify-content-center mt30">
            <div class="col-12 col-md-6 col-lg-3">
                <div class="icon_box_one">
                    <i class="fa fa-diamond"></i>
                    <div class="content">
                        <h4>Notre valeur</h4>
                        <p>
                            Une plongée dans le développement produit en flux
                        </p>
                        <!--a href="#">more</a-->
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <div class="icon_box_one">
                    <i class="fa fa-plane"></i>
                    <div class="content">
                        <h4>International</h4>
                        <p>
                            Les conférenciers viennent du monde entier
                        </p>
                        <!--a href="#">more</a-->
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <div class="icon_box_one">
                    <i class="fa fa-hourglass"></i>
                    <div class="content">
                        <h4>2 jours</h4>
                        <p>
                            2 jours de conférences dans une grande salle
                        </p>
                        <!--a href="#">more</a-->
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <div class="icon_box_one">
                    <i class="fa fa-th"></i>
                    <div class="content">
                        <h4>5 tracks</h4>
                        <p>
                            5 tracks avec 30+ conférences et ateliers
                        </p>
                        <!--a href="#">more</a-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
